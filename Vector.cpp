#include "Vector.h"

/*
	Constructor initializes the vector object.

	Input:
			n - The capacity and the resizeFactor.

	Output:
			Vector object.
*/
Vector::Vector(int n) :
	_size(0)
{
	if (n < 2)
	{
		n = 2;
	}

	this->_elements = new int[n];
	this->_resizeFactor = n;
	this->_capacity = n;
}

/*
	Destructor deallocates the elements in the vector.

	Input:
			void.

	Output:
			void.
*/
Vector::~Vector()
{
	delete[] this->_elements;
}

int Vector::size() const
{
	return this->_size;
}

int Vector::capacity() const
{
	return this->_capacity;
}

int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

int* Vector::elements() const
{
	return this->_elements;
}

/*
	Method checks if the vector is empty.

	Input:
			void.

	Output:
			True if the size of the vector is 0.
*/
bool Vector::empty() const
{
	return this->_size == 0;
}

/*
	Method adds element at the end of the vector.

	Input:
			val - The value to push.

	Output:
			void.
*/
void Vector::push_back(const int& val)
{
	if (this->_size == this->_capacity)
	{
		int* newArr = new int[this->_size + this->_resizeFactor];
		memcpy(newArr, this->_elements, sizeof(int) * this->_capacity);
		delete[] this->_elements;
		this->_elements = newArr;
		this->_capacity += this->_resizeFactor;
	}

	this->_elements[this->_size] = val;
	this->_size++;
}

/*
	Method deletes the last valid element in the vector and returns it.

	Input:
			void.

	Output:
			The deleted element of -9999 if empty.
*/
int Vector::pop_back()
{
	if (this->empty())
	{
		std::cerr << "error: pop from empty vector" << std::endl;
		return EMPTY_ERROR;
	}

	else
	{
		this->_size--;
		int tmp = this->_elements[this->_size];
		this->_elements[this->_size] = 0;
		return tmp;
	}
}

/*
	Method assures that the vector capacity >= n.

	Input:
			n - The new capacity value.

	Output:
			void.
*/
void Vector::reserve(int n)
{
	if (this->_capacity < n)
	{
		int numOfJumps = (int)ceil((n - this->_capacity) / this->_resizeFactor);
		this->_capacity += this->_resizeFactor * numOfJumps;
		
		int* newArr = new int[this->_capacity];
		memcpy(newArr, this->_elements, sizeof(int) * this->_size);
		
		delete[] this->_elements;
		this->_elements = newArr;
	}
}

/*
	Method changes the vector size to n.

	Input:
			n - The size to change to.

	Output:
			void.
*/
void Vector::resize(int n)
{
	if(n > this->_capacity)
	{
		this->reserve(n);
	}

	this->_size = n;
}

/*
	Method assigns value in all accessible elements in the vector.

	Input:
			n - The value to assign..

	Output:
			void.
*/
void Vector::assign(int val)
{
	memset(this->_elements, val, sizeof(int) * this->_size);
}

/*
	Method resizes the vector and assigns every element in it to val.

	Input:
			n - The new size.
			val - The value to assign.
	Output:
			void.
*/
void Vector::resize(int n, const int& val)
{
	this->resize(n);
	this->assign(val);
}

/*
	Constructor creates a deep copy of a vector.

	Input:
			other - The vector to copy from.
	Output:
			Vector object.
*/
Vector::Vector(const Vector& other) :
	_size(other._size), _capacity(other._capacity), _resizeFactor(other._resizeFactor)
{
	this->_elements = new int[_capacity];
	memcpy(this->_elements, other._elements, this->_size);
}

/*
	Operator= assigns a vector to another (shallow copy).

	Input:
			other - The vector to copy from.

	Output:
			The copied vector.
*/
Vector& Vector::operator=(const Vector& other)
{
	delete this;
	return *(new Vector(other));
}

/*
	Operator[] returns the (n + 1)th element of the vector.

	Input:
			n - The index of the input.

	Output:
			The element at index n or if n >= size it returns the 1st element.
*/
int& Vector::operator[](int n) const
{
	if (n >= this->_size)
	{
		std::cerr << "Index is out of range!" << std::endl;
		n = 0;
	}

	return this->_elements[n];
}
